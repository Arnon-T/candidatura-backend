package com.candidatura.candidatura.parte_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
public class CandidaturaController {

    private final CandidaturaService candidaturaService;

    @Autowired
    public CandidaturaController(CandidaturaService candidaturaService) { this.candidaturaService = candidaturaService; }

/*    1.	Dado uma lista de números, imprimir a lista de trás para a frente.
    GET http://localhost:8080/listaReversa?lista=25,35,40,21
    Resposta
    HTTP 200 OK {21,40,35,25}
*/

    @GetMapping("/listaReversa")
    public ResponseEntity<int[]> listaReversa(@RequestParam("lista") int listaOriginal[]){
        return new ResponseEntity<>(candidaturaService.listaReversa(listaOriginal), HttpStatus.OK) ;
    }


}
