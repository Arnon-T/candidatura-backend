use master;

CREATE TABLE remetente
(
id BIGINT IDENTITY(1,1)      PRIMARY KEY NOT NULL,
nome VARCHAR(50)                         NOT NULL,
telefone BIGINT                             NOT NULL,
cpf VARCHAR(11) NOT NULL,
endereco VARCHAR(144)                    NOT NULL,
email VARCHAR(50)                        NOT NULL
);

CREATE TABLE destinatario
(
    id BIGINT IDENTITY(1,1)      PRIMARY KEY NOT NULL,
    nome VARCHAR(50)                         NOT NULL,
    telefone BIGINT                             NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    endereco VARCHAR(144)                    NOT NULL,
    email VARCHAR(50)                        NOT NULL
);